<?php
/**
 * @file
 * Provides a Clients connection type for the SuiteCRM.
 *
 * @TODO Implement permissions handling.
 * @TODO Implement complex data types.
 * @TODO Implement relationship handling.
 * @TODO Implement suiteCRM portal user authentication / integration.
 * @TODO Figure out if a property or a shadowed field as priority when saving.
 * @TODO Add schema change handling.
 */

/**
 * Implements hook_menu().
 */
function clients_suitecrm_menu() {
  $items['admin/structure/suitecrm'] = array(
    'title' => 'SuiteCRM Entities',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('clients_suitecrm_entity_config_form'),
    'access arguments' => array('access administration pages'),
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'clients_suitecrm.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_clients_connection_type_info().
 */
function clients_suitecrm_clients_connection_type_info() {
  return array(
    'suitecrm' => array(
      'label'  => t('SuiteCRM REST Client'),
      'description' => t('Provides a connection to the SuiteCRM via REST.'),
      'class' => '\\Drupal\\clients_suitecrm\\Clients\\Connection\\SuiteCrm',
      'tests' => array(
        'connect' => '\\Drupal\\clients_suitecrm\\Clients\\Connection\\Test\\SuiteCrmConnection',
        'login' => '\\Drupal\\clients_suitecrm\\Clients\\Connection\\Test\\SuiteCrmLogin',
      ),
      'interfaces' => array(
        'ClientsRemoteEntityInterface',
      ),
    ),
  );
}

/**
 * Implements hook_flush_caches().
 */
function clients_suitecrm_flush_caches() {
  // Provide our own cache bin.
  return array('cache_suitecrm');
}

/**
 * Implements hook_list_option_info().
 */
function clients_suitecrm_list_option_info() {
  $info['clients_suitecrm_campaigns_options'] = array(
    'label' => t('SuiteCRM: Campaigns'),
    'callback' => 'clients_suitecrm_campaigns_options',
  );
  return $info;
}

/**
 * Callback to fetch Campaigns from the SuiteCRM.
 */
function clients_suitecrm_campaigns_options() {
  $options = array();
  $connections = clients_connection_load_all('suitecrm');
  /** @var \Drupal\clients_suitecrm\Clients\Connection\SuiteCrm $connection */
  foreach ($connections as $key => $connection) {
    // Skip substitutions.
    if (!clients_suitecrm_connection_is_for_environment($key, 'production')) {
      continue;
    }
    $options[$connection->label] = array();
    $connection->connect();
    $result = $connection->callMethodArray('get_entry_list', array(
      'module_name' => 'Campaigns',
      'query' => '',
      'order_by' => '',
      'offset'  => '0',
      'select_fields' => array(
        'id',
        'name',
        'date_entered',
      ),
      'link_name_to_fields_array' => array(),
      'max_results' => '50',
      'deleted' => '0',
      'favorites' => FALSE,
    ));
    if (!empty($result->entry_list)) {
      foreach ($result->entry_list as $campaign) {
        $options[$connection->label][$campaign->id] = $campaign->name_value_list->name->value;
      }
    }
  }
  return $options;
}

/**
 * Implements hook_clients_default_resources().
 */
function clients_suitecrm_clients_default_resources() {
  $default_resources = array();
  // Define all modules as possible resources.
  $connections = clients_connection_load_all('suitecrm');

  $entity_enabled_resources = variable_get('clients_suitecrm_enabled_entity_resources', array());

  /** @var \Drupal\clients_suitecrm\Clients\Connection\SuiteCrm $connection */
  foreach ($connections as $connection_name => $connection) {
    // Just create resources for non-substitute connections.
    if (clients_suitecrm_connection_is_for_environment($connection_name, 'production')) {
      if ($connection instanceof \Drupal\clients_suitecrm\Clients\Connection\SuiteCrm) {
        // Fetch the label if the non substituted connection if necessary.
        $label = $connection->label;
        if (($original_connection_name = clients_connection_get_substituted_name($connection_name)) && $original_connection_name != $connection_name) {
          $original_connection = entity_load_single('clients_connection', $original_connection_name);
          $label = $original_connection->label;
        }
        if (($result = clients_suitecrm_get_resource_module_list($connection_name))) {
          foreach ($result as $module) {
            $entity_type = strtolower($connection_name . '__' . $module->module_key);
            $module_label = (!empty($module->module_label)) ? $module->module_label : $module->module_key;

            // Wired but seems the most elegant way atm.
            $default_resources[$entity_type] = entity_import('clients_resource', entity_var_json_export(array(
              'remoteEntityQuery' => NULL,
              'connection' => $connection_name,
              'component' => $entity_type,
              'name' => $entity_type,
              'label' => $label . ': ' . $module_label,
              'type' => 'remote_entity',
              'configuration' => array(
                'module' => drupal_json_decode(entity_var_json_export(clone $module)),
              ),
            )));

            // If this is an entity enabled resource, store all the information
            // about the fields in the configuration.
            if (!empty($entity_enabled_resources[$connection_name][$entity_type])) {
              // Generate the entity info based on the already acquired
              // configuration and store to the configuration.
              $default_resources[$entity_type]->configuration['entity info'] = clients_suitecrm_crm_to_entity_info($default_resources[$entity_type]);
            }
          }
        }
      }
    }
  }
  return $default_resources;
}

/**
 * Implements hook_ENTITY_TYPE_update() for clients_resource_base.
 */
function clients_suitecrm_clients_connection_insert($entity) {
  // Ensure the default client resources are rebuilt.
  clients_suitecrm_clients_connection_update($entity);
}

/**
 * Implements hook_ENTITY_TYPE_update() for clients_resource_base.
 */
function clients_suitecrm_clients_connection_update($entity) {
  // Ensure the default client resources are rebuilt.
  if ($entity->type == 'suitecrm') {
    // Flush suitecrm cache.
    cache_clear_all(NULL, 'cache_suitecrm');
    cache_clear_all('*', 'cache_suitecrm', TRUE);
    static $registered;
    if (!isset($registered)) {
      $registered = TRUE;
      drupal_register_shutdown_function('clients_suitecrm_rebuild_client_resources');
    }
  }
}

/**
 * Rebuild client resources.
 */
function clients_suitecrm_rebuild_client_resources() {
  entity_defaults_rebuild(array('clients_resource'));
}

/**
 * Implements hook_ENTITY_TYPE_presave() for clients_resource_base.
 */
function clients_suitecrm_clients_resource_presave($entity) {

  // Ensure the entity info is up to date.
  if ($entity instanceof clients_resource_remote_entity && ($connection = $entity->getConnection()) && $connection instanceof \Drupal\clients_suitecrm\Clients\Connection\SuiteCrm) {
    $entity_enabled_resources = variable_get('clients_suitecrm_enabled_entity_resources', array());
    // Ensure we use the original name if this is a substitution.
    $connection_name = clients_connection_get_substituted_name($connection->name);
    if (isset($entity_enabled_resources[$connection_name][$entity->identifier()])) {
      $entity->configuration['entity info'] = clients_suitecrm_crm_to_entity_info($entity);

      // If the entity info was changed clear the related caches.
      $original_entity_info = (isset($entity->original->configuration['entity info'])) ? $entity->original->configuration['entity info'] : NULL;
      if ($original_entity_info != $entity->configuration['entity info']) {
        entity_info_cache_clear();
        entity_property_info_cache_clear();
        drupal_get_complete_schema(TRUE);
        menu_rebuild();
      }

      // Create the entity table if it doesn't exist yet and entity info are
      // available.
      if (!empty($entity->configuration['entity info'])) {
        $table = $entity->configuration['entity info']['base table'];
        if (!db_table_exists($table)) {
          $schema = clients_suitecrm_crm_to_entity_schema($entity);
          db_create_table($table, $schema);
        }
      }
    }
  }
}

/**
 * Removes all base tables of non entity client resources.
 *
 * @return array
 *   List of all dropped tables.
 */
function clients_suitecrm_entity_cleanup() {
  $dropped = array();

  $connections = clients_connection_load_all('suitecrm');

  $efq = new EntityFieldQuery('clients_resource');
  $result = $efq
    ->entityCondition('entity_type', 'clients_resource')
    ->propertyCondition('connection', array_keys($connections))
    ->execute();

  $clients_suitecrm_enabled_entity_resources = variable_get('clients_suitecrm_enabled_entity_resources', array());
  if (!empty($result['clients_resource'])) {
    $client_resources = entity_load_multiple_by_name('clients_resource', array_keys($result['clients_resource']));
    foreach ($client_resources as $client_resource) {
      if (!isset($clients_suitecrm_enabled_entity_resources[$client_resource->connection][$client_resource->identifier()]) && db_table_exists($client_resource->component)) {
        $dropped[$client_resource->component] = $client_resource->component;
        db_drop_table($client_resource->component);
      }
    }
  }

  return $dropped;
}

/**
 * Provide an entity type via the remote entity API.
 *
 * @see remote_entity_hook_entity_info()
 * @see hook_entity_info()
 * @see entity_crud_hook_entity_info()
 */
function clients_suitecrm_entity_info() {
  $info = array();

  // Fetch the entity info from the clients resource configuration. The entity
  // info is pre-generated because we rely on other entities to build it.
  // But in hook_entity_info() we aren't allowed to call other entity API
  // functions.
  $connection_entity_resources = variable_get('clients_suitecrm_enabled_entity_resources', array());
  foreach ($connection_entity_resources as $connection_name => $entity_resources) {
    foreach ($entity_resources as $entity_resource) {
      $data = db_select('clients_resource')
        ->fields('clients_resource')
        ->condition('name', $entity_resource)
        ->execute()
        ->fetch();
      if (!empty($data->configuration)) {
        // As we read from the DB we've to take care of unserializing.
        $data->configuration = unserialize($data->configuration);
        if (!empty($data->configuration['entity info'])) {
          $info[$data->component] = $data->configuration['entity info'];

          // Create the entity table if it doesn't exist yet.
          $table = $data->configuration['entity info']['base table'];
          if (!db_table_exists($table)) {
            $schema = clients_suitecrm_crm_to_entity_schema($data);
            db_create_table($table, $schema);
          }
        }
      }
    }
  }
  return $info;
}

/**
 * Implements hook_entity_operation_info().
 */
function clients_suitecrm_entity_operation_info() {
  $info = array();
  $connection_entity_resources = variable_get('clients_suitecrm_enabled_entity_resources', array());
  foreach ($connection_entity_resources as $connection_name => $entity_resources) {
    foreach ($entity_resources as $entity_resource) {
      $info[$entity_resource] = array(
        'add' => array(
          'handler' => '\\Drupal\\clients_suitecrm\\EntityOperationsOperation\\Add',
          'provision' => array(
            'menu' => TRUE,
          ),
        ),
        'fetch' => array(
          'handler' => '\\Drupal\\clients_suitecrm\\EntityOperationsOperation\\Fetch',
          'provision' => array(
            'menu' => TRUE,
          ),
        ),
        'view' => array(
          // Or try EntityOperationsOperationEntityViewOperations!
          'handler' => 'EntityOperationsOperationEntityView',
          'provision' => array(
            'menu' => array(
              'default' => TRUE,
            ),
            'views field' => TRUE,
          ),
        ),
        'edit' => array(
          'handler' => '\\Drupal\\clients_suitecrm\\EntityOperationsOperation\\Edit',
          'provision' => array(
            'menu' => TRUE,
            'views field' => TRUE,
          ),
        ),
        'delete' => array(
          'handler' => 'EntityOperationsOperationDelete',
          'provision' => array(
            'menu' => TRUE,
            'views field' => TRUE,
          ),
        ),
        'devel/devel' => array(
          'handler' => 'EntityOperationsOperationDevel',
          'provision' => array(
            'menu' => array(
              'default secondary' => TRUE,
            ),
          ),
        ),
      );
    }
  }
  return $info;
}

/**
 * Property shadowing handler for SuiteCRM datetime type.
 *
 * Converts incoming values to Unix timestamps.
 * Converts outgoing values to date strings: Y-m-d H:i:s
 *
 * @see remote_entity_shadowing_schema_property_verbatim()
 */
function clients_suitecrm_shadowing_schema_property_verbatim_datetime($entity, $property_name, $entity_type, $property_info, $direction) {
  $local_property_name = $property_info['remote property shadowing']['local property'];
  $wrapper = entity_metadata_wrapper($entity_type, $entity);

  if ($direction == 'local to remote') {
    // Get a value from the schema property and convert it to date string.
    $value = $wrapper->{$local_property_name}->raw();
    if ($value) {
      $date = new DateTime('@' . $value);
    }
    else {
      $date = new DateTime();
    }
    return $date->format('Y-m-d H:i:s');
  }

  if ($direction == 'remote to local') {
    // Get the value from the remote property and convert it to a unixtimestmap.
    $value = $wrapper->{$property_name}->raw();
    $date = new DateTime($value);
    $entity->{$local_property_name} = (int) $date->format('U');
  }
}

/**
 * Property shadowing handler for SuiteCRM date type.
 *
 *  Converts incoming values to Unix timestamps.
 * Converts outgoing values to date strings: Y-m-d H:i:s
 *
 * @see remote_entity_shadowing_schema_property_verbatim()
 */
function clients_suitecrm_shadowing_schema_property_verbatim_date($entity, $property_name, $entity_type, $property_info, $direction) {
  $local_property_name = $property_info['remote property shadowing']['local property'];
  $wrapper = entity_metadata_wrapper($entity_type, $entity);

  if ($direction == 'local to remote') {
    // Get a value from the schema property and convert it to date string.
    $value = $wrapper->{$local_property_name}->raw();
    $date = new DateTime('@' . $value);
    return $date->format('Y');
  }

  if ($direction == 'remote to local') {
    // Get the value from the remote property and convert it to a unixtimestmap.
    $value = $wrapper->{$property_name}->raw();
    $date = new DateTime($value . '-01-01');
    $entity->{$local_property_name} = (int) $date->format('U');
  }
}

/**
 * Converts suiteCRM module metadata to cms metadata.
 *
 * @param clients_resource_base $clients_resource
 *   The resource to analyze.
 *
 * @return array
 *   Array with the single metadata parts:
 *   - schema: The schema for the table.
 *   - entity info: The entity info declaration for this resource.
 *   - fields: The field base and instance configurations.
 */
function clients_suitecrm_crm_ressource_to_cms($clients_resource) {
  $schema = clients_suitecrm_crm_to_entity_schema($clients_resource);
  $entity_info = clients_suitecrm_crm_to_entity_info($clients_resource, $schema);
  return array(
    'schema' => $schema,
    'entity info' => $entity_info,
  );
}

/**
 * Provides the table schema based on the fields metadata from suite.
 *
 * @param clients_resource_base $clients_resource
 *   The resource to analyze.
 *
 * @return array
 *   The table schema.
 */
function clients_suitecrm_crm_to_entity_schema($clients_resource) {
  module_load_install('remote_entity');
  $schema = remote_entity_schema_table($clients_resource->component);
  // Avoid notices in case we don't get any fields. Such a case results in an
  // raw base table.
  if (!empty($clients_resource->configuration['fields']['module_fields'])) {
    foreach ($clients_resource->configuration['fields']['module_fields'] as $field) {
      if ($field_schema = clients_suitecrm_crm_to_field_schema($field)) {
        $schema['fields'] += $field_schema;
      }
    }
  }
  // Allow other modules to adjust the schema.
  drupal_alter('clients_suitecrm_schema', $schema, $clients_resource);
  return $schema;
}

/**
 * Converts the information from an get_module_fields() item to a schema array.
 *
 * @return array|FALSE
 *   The array with the field schema or FALSE if this isn't a property.
 */
function clients_suitecrm_crm_to_field_schema($suitecrm_field) {
  $field_schema = array();

  // Prefix all remote properties with crm.
  $field_name = 'crm_' . $suitecrm_field['name'];

  // The id field is always the remote_id field in our database.
  if ($field_name == 'crm_id') {
    $field_name = 'remote_id';
  }

  $field_schema[$field_name]['description'] = 'SuiteCRM Type: ' . $suitecrm_field['type'];

  switch ($suitecrm_field['type']) {
    case 'datetime':
    case 'date':
      $field_schema[$field_name] += array(
        'type' => 'int',
        'size' => 'big',
      );
      break;

    case 'text':
      $field_schema[$field_name] += array(
        'type' => 'text',
        'size' => 'big',
      );
      // Longtext fields can't have a default value.
      unset($suitecrm_field['default_value']);
      break;

    case 'bool':
      $field_schema[$field_name] += array(
        'type' => 'int',
        'size' => 'tiny',
      );
      break;

    case 'id':
    case 'name':
    case 'phone':
    case 'email':
    case 'url':
    case 'varchar':
      $field_schema[$field_name] += array(
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => '',
        'length' => 255,
      );
      break;

    case 'enum':
      $field_schema[$field_name] += array(
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => '',
        'length' => 255,
      );
      if (isset($suitecrm_field['options'])) {
        // Add custom metadata to re-use in property info.
        $field_schema[$field_name]['suite_options'] = array();
        foreach ($suitecrm_field['options'] as $option => $option_data) {
          $field_schema[$field_name]['suite_options'][$option] = $option_data['value'];
        }
      }
      break;

    case 'assigned_user_name':
    case 'relate':
    default:
      return FALSE;
  }

  if (!empty($suitecrm_field['len'])) {
    $field_schema[$field_name]['length'] = $suitecrm_field['len'];
  }
  if (!empty($suitecrm_field['label'])) {
    $field_schema[$field_name]['description'] = $suitecrm_field['label'];
  }
  if (isset($suitecrm_field['default_value'])) {
    $field_schema[$field_name]['default'] = $suitecrm_field['default_value'];
    // Numeric defaults should be casted to the matching type.
    if (is_numeric($field_schema[$field_name]['default']) || $field_schema[$field_name]['type'] == 'int') {
      // Use float so we don't loose decimals.
      $field_schema[$field_name]['default'] = (float) $field_schema[$field_name]['default'];
    }
  }
  return $field_schema;
}

/**
 * Provides the entity info based on the fields metadata from suite.
 *
 * @param clients_resource_base $clients_resource
 *   The resource to analyze.
 *
 * @return array
 *   The table schema.
 */
function clients_suitecrm_crm_to_entity_info($clients_resource, $schema = NULL) {
  // Inject the field information provided by suite.
  if (!empty($clients_resource->configuration['module']['module_key'])) {
    if (($fields = clients_suitecrm_get_ressource_field_list($clients_resource))) {
      if (!empty($clients_resource->configuration)) {
        $configuration = $clients_resource->configuration;
      }
      $configuration['fields'] = clone $fields;
      $clients_resource->configuration = drupal_json_decode(entity_var_json_export($configuration));
    }
  }

  if (empty($schema)) {
    $schema = clients_suitecrm_crm_to_entity_schema($clients_resource);
  }

  // Collect remote properties from the schema.
  $property_map = array('remote_id' => 'id');
  foreach ($schema['fields'] as $field_name => $field_schema) {
    if (strpos($field_name, 'crm_') === 0) {
      $remote_property = substr($field_name, 4);
      $property_map['s_' . $remote_property] = $remote_property;
    }
  }
  // Register custom additional properties.
  if (!empty($clients_resource->configuration['additional_properties']) && is_array($clients_resource->configuration['additional_properties'])) {
    $additional_properties = $clients_resource->configuration['additional_properties'];
    foreach ($additional_properties as $additional_property) {
      $property_map[$additional_property['local_property']] = $additional_property['remote_property'];
    }
  }
  $entity_info = array(
    'label' => $clients_resource->label,
    'entity class' => '\\Drupal\\clients_suitecrm\\Entity\\SuiteCrm',
    'controller class' => '\\Drupal\\clients_suitecrm\\Controller\\SuiteCrmController',
    'metadata controller class' => 'Drupal\\clients_suitecrm\\Entity\\MetaDataController\\SuiteCrm',
    'base table' => $clients_resource->component,
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'eid',
      'bundle' => 'type',
      'needs remote save' => 'needs_remote_save',
    ),
    'bundles' => array(
      $clients_resource->component => array(
        'label' => $clients_resource->label,
        'remote entity conditions' => array(
          'type' => $clients_resource->component,
        ),
        'admin' => array(
          'path' => 'admin/structure/suitecrm/' . $clients_resource->component,
        ),
      ),
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'view modes' => array(
      'full' => array(
        'label' => t('Full content'),
        'custom settings' => FALSE,
      ),
    ),
    'module' => 'clients_suitecrm',
    'access callback' => 'clients_suitecrm_entity_access',
    // Remote Entity API properties.
    'remote base table' => $clients_resource->configuration['module']['module_key'],
    'property map' => $property_map,
    'remote entity keys' => array(
      'remote id' => 'id',
    ),
    'expiry' => array(
      'expiry time' => 3600,
    ),
    // Admin UI properties.
    'admin ui' => array(
      'path' => 'admin/structure/suitecrm/' . $clients_resource->component,
      // We need our own controller for this, because we're using generic
      // entity form operations.
      'controller class' => 'EntityOperationsDefaultAdminUIController',
    ),
    // Entity Operations API.
    'operations ui' => array(
      // The base path for your entities. This is the same as your entity's URI
      // but without the ID suffix. (In fact, you can set
      // entity_operations_entity_uri() as your URI callback, which will use the
      // value here).
      'path' => 'suitecrm/' . $clients_resource->component,
    ),
  );
  // Allow other modules to extend / adjust the entity info.
  drupal_alter('clients_suitecrm_entity_info', $entity_info, $clients_resource, $schema);
  return $entity_info;
}

/**
 * Entity access callback.
 *
 * @TODO Actually implement. SuiteCRM ACL based?
 */
function clients_suitecrm_entity_access() {
  return TRUE;
}

/**
 * Declare information about remote tables for RemoteEntityQuery.
 *
 * Loosely inspired by hook_views_data().
 *
 * WARNING: This system is currently under active development, and may be
 * further changed in the future.
 *
 * @return array
 *   An array of data about remote tables, used for building remote queries.
 *   This should be keyed by connection name, where each value is then an array
 *   keyed by remote table name. Each table value is an array of data for that
 *   table containing:
 *   - 'fields': An array whose keys are remote field names. Each value is an
 *     array which itself contains data about the field. The contents of this
 *     will depend on the connection type.
 *   - 'relationships': The contents of this depend on the connection type.
 *
 * @see remote_entity_get_query_table_info()
 * @see hook_remote_entity_query_table_info_alter()
 */
function clients_suitecrm_remote_entity_query_table_info() {
  $data = array();

  $entity_enabled_resources = variable_get('clients_suitecrm_enabled_entity_resources', array());
  foreach ($entity_enabled_resources as $connection_name => $client_resources) {
    foreach ($client_resources as $client_resource_name) {
      $clients_resource = clients_resource_load($client_resource_name);
      if (!empty($clients_resource->configuration['fields']['module_fields'])) {
        $remote_table_name = $clients_resource->configuration['module']['module_key'];
        foreach ($clients_resource->configuration['fields']['module_fields'] as $field) {
          $data[$connection_name][$remote_table_name]['fields'][$field['name']] = array(
            'type' => $field['type'],
          );
          // @TODO Handle relationships.
        }
      }
    }
  }
  return $data;
}

/**
 * Implements hook_form_FORM_ID_alter() for clients_resource_form().
 *
 * @see clients_resource_form()
 * @see clients_suitecrm_form_clients_resource_form_alter_submit()
 */
function clients_suitecrm_form_clients_resource_form_alter(&$form, &$form_state) {
  // If this is a resource with a SuiteCrm connection add some extended
  // configuration.
  if (!empty($form_state['clients_resource']->connection) && $form_state['clients_resource'] instanceof clients_resource_remote_entity) {
    $connection = $form_state['clients_resource']->getConnection();
    if ($connection instanceof \Drupal\clients_suitecrm\Clients\Connection\SuiteCrm) {
      if (!isset($form_state['clients_resource']->configuration)) {
        $form_state['clients_resource']->configuration = array();
      }
      $configuration = $form_state['clients_resource']->configuration + array(
        'additional_properties' => '',
        'remote_methods' => array(
          'select' => '',
          'insert' => '',
          'update' => '',
        ),
        'module' => array('module_key' => ''),
      );
      $form['configuration']['additional_properties'] = array(
        '#type' => 'textarea',
        '#title' => t('Extra remote properties'),
        '#description' => t('Allows you to define additional remote properties. Format is local_property_name|remote_property|default_value. Additional properties are all handled as text.'),
        '#default_value' => $configuration['additional_properties'],
        '#process' => array('clients_suitecrm_form_clients_resource_form_alter_additional_properties_process'),
        '#value_callback' => 'clients_suitecrm_form_clients_resource_form_alter_additional_properties_value',
      );
      $form['configuration']['module']['module_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Module key'),
        '#description' => t('The module key to use in the service calls. Leave empty for default.'),
        '#default_value' => $configuration['module']['module_key'],
      );
      $form['configuration']['remote_methods'] = array(
        '#type' => 'fieldset',
        '#title' => 'Custom REST methods',
        '#description' => 'Allows you to define custom remote methods for this resource. Leave empty to use default.',
      );
      $form['configuration']['remote_methods']['select'] = array(
        '#type' => 'textfield',
        '#title' => t('Select'),
        '#description' => t('Has to be compatible with !link. Leave empty for default.', array(
          '!link' => l(t('get_entry_list'), 'http://support.sugarcrm.com/02_Documentation/04_Sugar_Developer/Sugar_Developer_Guide_6.5/02_Application_Framework/Web_Services/05_Method_Calls/get_entry_list'),
        )),
        '#default_value' => $configuration['remote_methods']['select'],
      );
      $form['configuration']['remote_methods']['insert'] = array(
        '#type' => 'textfield',
        '#title' => t('Insert'),
        '#description' => t('Has to be compatible with !link.  Leave empty for default.', array(
          '!link' => l(t('set_entry'), 'http://support.sugarcrm.com/02_Documentation/04_Sugar_Developer/Sugar_Developer_Guide_6.5/02_Application_Framework/Web_Services/05_Method_Calls/set_entry'),
        )),
        '#default_value' => $configuration['remote_methods']['insert'],
      );
      $form['configuration']['remote_methods']['update'] = array(
        '#type' => 'textfield',
        '#title' => t('Update'),
        '#description' => t('Has to be compatible with !link. Leave empty for default.', array(
          '!link' => l(t('set_entry'), 'http://support.sugarcrm.com/02_Documentation/04_Sugar_Developer/Sugar_Developer_Guide_6.5/02_Application_Framework/Web_Services/05_Method_Calls/set_entry'),
        )),
        '#default_value' => $configuration['remote_methods']['update'],
      );
    }
  }
}

/**
 * Element processor to ensure the additional properties are set as string.
 *
 * @see clients_suitecrm_form_clients_resource_form_alter()
 * @see clients_suitecrm_form_clients_resource_form_alter_additional_properties_value()
 */
function clients_suitecrm_form_clients_resource_form_alter_additional_properties_process($element, &$form_state) {
  // Ensure the $value is a proper array.
  if (!empty($element['#value']) && is_array($element['#value'])) {
    foreach ($element['#value'] as $key => $additional_property) {
      $element['#value'][$key] = implode('|', $additional_property);
    }
    $element['#value'] = implode("\n", $element['#value']);
  }
  return $element;
}

/**
 * Element value callback to ensure the additional properties are an array.
 *
 * @see clients_suitecrm_form_clients_resource_form_alter()
 * @see clients_suitecrm_form_clients_resource_form_alter_additional_properties_process()
 */
function clients_suitecrm_form_clients_resource_form_alter_additional_properties_value($element, $input = FALSE, $form_state = array()) {
  // Check what to use as input, the default value or given user input.
  if ($input === FALSE) {
    $additional_properties = isset($element['#default_value']) ? $element['#default_value'] : array();
  }
  else {
    $additional_properties = $input;
  }

  // Ensure this always returns a parsed array of additional properties.
  if (is_string($additional_properties) && !empty($additional_properties)) {
    $additional_properties = explode("\n", $additional_properties);
    foreach ($additional_properties as $key => $additional_property_string) {
      $values = explode('|', trim($additional_property_string));
      // Ensure all three values are set.
      $values = array_replace(array('', '', ''), $values);
      $additional_properties[$key] = array_combine(
        array('local_property', 'remote_property', 'default_value'),
        $values
      );
    }
  }
  return $additional_properties;
}

/**
 * Check if a connection is meant to be used in an environment.
 *
 * @param string $connection_name
 *   The name of a connection in use.
 * @param string $check_environment_name
 *   The environment name to check for, defaults to current environment.
 *
 * @return bool
 *   Returns TRUE if the connection is meant to be used in in an environment.
 */
function clients_suitecrm_connection_is_for_environment($connection_name, $check_environment_name = NULL) {
  // Normalize $check_environment_name.
  $environment_name = variable_get('environment_name', NULL);
  if (empty($check_environment_name)) {
    $check_environment_name = $environment_name;
  }
  if ($check_environment_name == 'production') {
    $check_environment_name = NULL;
  }

  $cache = &drupal_static(__FUNCTION__, array());
  if (isset($cache[$connection_name][$check_environment_name])) {
    return $cache[$connection_name][$check_environment_name];
  }

  $cache[$connection_name][$check_environment_name] = FALSE;
  $substituted_connection = FALSE;
  if (($env_split = strrpos($connection_name, '_'))) {
    $substituted_connection = substr($connection_name, 0, $env_split);
    $result = db_query("SELECT name FROM {clients_connection} WHERE name = :connection_name", array(
      ':connection_name' => $substituted_connection,
    ));
    $substituted_connection = $result->fetchField();
  }

  // If this is production and the connection isn't a substitute return TRUE;
  if (!isset($check_environment_name) && $substituted_connection == FALSE) {
    $cache[$connection_name][$check_environment_name] = TRUE;
  }
  // If this is an environment check if the connection belongs to the env.
  elseif (isset($check_environment_name) && substr($connection_name, strlen($substituted_connection) + 1) == $check_environment_name) {
    $cache[$connection_name][$check_environment_name] = TRUE;
  }
  return $cache[$connection_name][$check_environment_name];
}

/**
 * Fetch available modules for resources.
 *
 * If the call fails fall back to our "cached" List of modules. That way a
 * broken connection during rebuild of the resources won't break the rebuild.
 *
 * @param string $connection_name
 *   The connection name
 *
 * @return array|FALSE
 *   List of available modules.
 */
function clients_suitecrm_get_resource_module_list($connection_name) {

  // Ensure environment specific connection is used for calls.
  /** @var \Drupal\clients_suitecrm\Clients\Connection\SuiteCrm $connection */
  $env_connection = clients_connection_load($connection_name);
  $env_connection->connect();

  if (!($result = $env_connection->get_available_modules()) || empty($result)) {
    $clients_suitecrm_connection_resources = variable_get('clients_suitecrm_connection_resources', array());
    if (isset($clients_suitecrm_connection_resources[$connection_name])) {
      $result = $clients_suitecrm_connection_resources[$connection_name];
    }
  }
  else {
    // Update the stored list of modules.
    $clients_suitecrm_connection_resources = variable_get('clients_suitecrm_connection_resources', array());
    $result = $clients_suitecrm_connection_resources[$connection_name] = reset($result);
    variable_set('clients_suitecrm_connection_resources', $clients_suitecrm_connection_resources);
  }

  return $result;
}

/**
 * Fetch available fields for a resource.
 *
 * If the call fails fall back to our "cached" List of modules. That way a
 * broken connection during rebuild of the resources won't break the rebuild.
 *
 * @param clients_resource_remote_entity $clients_resource
 *   The clients resource.
 *
 * @return object|FALSE
 *   List of available fields.
 */
function clients_suitecrm_get_ressource_field_list($clients_resource) {

  if (!($fields = $clients_resource->getConnection()->callMethodArray('get_module_fields', array('module_name' => $clients_resource->configuration['module']['module_key']))) || empty($fields)) {
    $clients_suitecrm_connection_resources_fields = variable_get('clients_suitecrm_connection_resources_fields', array());
    if (isset($clients_suitecrm_connection_resources_fields[$clients_resource->name])) {
      $fields = $clients_suitecrm_connection_resources_fields[$clients_resource->name];
    }
  }
  else {
    // Update the stored list of fields.
    $clients_suitecrm_connection_resources_fields = variable_get('clients_suitecrm_connection_resources_fields', array());
    $clients_suitecrm_connection_resources_fields[$clients_resource->name] = $fields;
    variable_set('clients_suitecrm_connection_resources_fields', $clients_suitecrm_connection_resources_fields);
  }

  return $fields;
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function clients_suitecrm_clients_connection_delete($entity) {
  // Update the stored list of modules.
  $clients_suitecrm_connection_resources = variable_get('clients_suitecrm_connection_resources', array());
  unset($clients_suitecrm_connection_resources[$entity->name]);
  variable_set('clients_suitecrm_connection_resources', $clients_suitecrm_connection_resources);
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function clients_suitecrm_clients_resource_delete($entity) {
  // Update the stored list of fields.
  $clients_suitecrm_connection_resources_fields = variable_get('clients_suitecrm_connection_resources_fields', array());
  unset($clients_suitecrm_connection_resources_fields[$entity->name]);
  variable_set('clients_suitecrm_connection_resources_fields', $clients_suitecrm_connection_resources_fields);
}

/**
 * Options list callback for enum fields.
 *
 * Called by EntityMetadataWrapper::optionsList()
 */
function clients_suitecrm_enum_options_list($name, $info, $op) {
  if (isset($info['parent']) && $info['parent'] instanceof EntityDrupalWrapper) {
    $entity_info = $info['parent']->entityInfo();
    $schema = drupal_get_schema($entity_info['base table']);
    $local_field = $name;
    if (isset($info['remote property shadowing']['local property'])) {
      $local_field = $info['remote property shadowing']['local property'];
    }
    if (isset($schema['fields'][$local_field]['suite_options'])) {
      return $schema['fields'][$local_field]['suite_options'];
    }
  }
  return array();
}